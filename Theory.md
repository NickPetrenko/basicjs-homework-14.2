1. В чому відмінність між setInterval та setTimeout?
setTimeout виконує заданий код один раз після затримки, вказаної у мілісекундах.
Після виконання коду він завершується. 
setInterval виконує заданий код через заданий інтервал часу, також вказаний у 
мілісекундах. Код, переданий в setInterval, виконується повторно через вказаний 
інтервал часу, до тих пір, поки не буде викликано clearInterval або не буде закрито 
вікно браузера.


2. Чи можна стверджувати, що функції в setInterval та setTimeout будуть виконані 
рівно через той проміжок часу, який ви вказали?
-


3. Як припинити виконання функції, яка була запланована для виклику з використанням 
setTimeout та setInterval?
Використати clearTimeout чи clearInterval.