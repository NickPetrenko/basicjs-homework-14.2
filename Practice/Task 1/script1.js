//Створіть HTML-файл із кнопкою та елементом div.
//При натисканні кнопки використовуйте setTimeout, щоб змінити текстовий вміст 
//елемента div через затримку 3 секунди. Новий текст повинен вказувати, що 
//операція виконана успішно.

document.addEventListener('DOMContentLoaded', function() {
    var changeTextBtn = document.getElementById('changeTextBtn');
    var resultDiv = document.getElementById('result');
    
    changeTextBtn.addEventListener('click', function() {
        setTimeout(function() {
            resultDiv.textContent = 'Операція виконана успішно';
        }, 3000);
    });
});