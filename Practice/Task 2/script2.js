//Реалізуйте таймер зворотного відліку, використовуючи setInterval. При завантаженні 
//сторінки виведіть зворотний відлік від 10 до 1 в елементі div. Після досягнення 1 
//змініть текст на "Зворотній відлік завершено".

document.addEventListener('DOMContentLoaded', function() {
    var timerDisplay = document.getElementById('timer');
    
    var countdown = 10;
    
    timerDisplay.textContent = countdown;
    
    var timerInterval = setInterval(function() {
        countdown--;
        
        timerDisplay.textContent = countdown;
        
        if (countdown === 1) {
            timerDisplay.textContent = 'Зворотній відлік завершено';
            
            clearInterval(timerInterval);
        }
    }, 1000);
});